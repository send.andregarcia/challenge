define({ "api": [
 {
    "type": "get",
    "url": "regiao/{regiao}/{municipio}",
    "title": "Buscar municipio por Região",
    "name": "buscar_por_região",
    "group": "Challenge",
    "parameter":  {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "regiao",
            "description": "<p>Nome da região onde será feita a busca</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "municipio",
            "description": "<p>Nome do municipio a ser buscado</p>"
          }
        ]
      }
      },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",

            "content":"[\r\n  {\r\n    \"id\": 5101,\r\n    \"cod\": \"4323754\",\r\n    \"municipio\": \"Vit\u00F3ria das Miss\u00F5es\",\r\n    \"uf\": \"RS\"\r\n  },\r\n  {\r\n    \"id\": 5101,\r\n    \"cod\": \"4323754\",\r\n    \"municipio\": \"Vit\u00F3ria das Miss\u00F5es\",\r\n    \"uf\": \"RS\"\r\n  },\r\n  {\r\n    \"id\": 5101,\r\n    \"cod\": \"4323754\",\r\n    \"municipio\": \"Vit\u00F3ria das Miss\u00F5es\",\r\n    \"uf\": \"RS\"\r\n  },\r\n  {\r\n    \"id\": 4978,\r\n    \"cod\": \"4317301\",\r\n    \"municipio\": \"Santa Vit\u00F3ria do Palmar\",\r\n    \"uf\": \"RS\"\r\n  }\r\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "[]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "",
    "groupTitle": "Challenge"
   },{
            "type": "get",
            "url": "estado/{estado}/{municipio}",
            "title": "Buscar municipio por Estado",
            "name": "buscar_por_estado",
            "group": "Challenge",
            "parameter":  {
                "fields": {
                    "Parameter": [
                        {
                            "group": "Parameter",
                            "type": "string",
                            "optional": false,
                            "field": "estado",
                            "description": "<p>Nome do Estado onde será feita a busca</p>"
                        },
                        {
                            "group": "Parameter",
                            "type": "string",
                            "optional": false,
                            "field": "municipio",
                            "description": "<p>Nome do municipio a ser buscado</p>"
                        }
                    ]
                }
            },
            "success": {
                "examples": [
                    {
                        "title": "Success-Response:",

                        "content":"[\r\n  {\r\n    \"id\": 5101,\r\n    \"cod\": \"4323754\",\r\n    \"municipio\": \"Vit\u00F3ria das Miss\u00F5es\",\r\n    \"uf\": \"RS\"\r\n  },\r\n  {\r\n    \"id\": 5101,\r\n    \"cod\": \"4323754\",\r\n    \"municipio\": \"Vit\u00F3ria das Miss\u00F5es\",\r\n    \"uf\": \"RS\"\r\n  },\r\n  {\r\n    \"id\": 5101,\r\n    \"cod\": \"4323754\",\r\n    \"municipio\": \"Vit\u00F3ria das Miss\u00F5es\",\r\n    \"uf\": \"RS\"\r\n  },\r\n  {\r\n    \"id\": 4978,\r\n    \"cod\": \"4317301\",\r\n    \"municipio\": \"Santa Vit\u00F3ria do Palmar\",\r\n    \"uf\": \"RS\"\r\n  }\r\n]",
                        "type": "json"
                    }
                ]
            },
            "error": {
                "examples": [
                    {
                        "title": "Error-Response:",
                        "content": "[]",
                        "type": "json"
                    }
                ]
            },
            "version": "0.0.0",
            "filename": "",
            "groupTitle": "Challenge"
        },
        {
            "type": "get",
            "url": "estados",
            "title": "Listar Estados",
            "name": "retornar_estados",
            "group": "Challenge",
            "success": {
                "examples": [
                    {
                        "title": "Success-Response:",
                        "content":"[\r\n  {\r\n    \"id\": 1,\r\n    \"cod\": 12,\r\n    \"estado\": \"Acre\",\r\n    \"sigla\": \"AC\",\r\n    \"regiao\": 1\r\n  },\r\n  {\r\n    \"id\": 2,\r\n    \"cod\": 27,\r\n    \"estado\": \"Alagoas\",\r\n    \"sigla\": \"AL\",\r\n    \"regiao\": 2\r\n  },\r\n  {\r\n    \"id\": 3,\r\n    \"cod\": 16,\r\n    \"estado\": \"Amap\u00E1\",\r\n    \"sigla\": \"AP\",\r\n    \"regiao\": 1\r\n  },\r\n  {\r\n    \"id\": 4,\r\n    \"cod\": 13,\r\n    \"estado\": \"Amazonas\",\r\n    \"sigla\": \"AM\",\r\n    \"regiao\": 1\r\n  }\r\n]",
                        "type": "json"
                    }
                ]
            },
            "error": {
                "examples": [
                    {
                        "title": "Error-Response:",
                        "content": "[]",
                        "type": "json"
                    }
                ]
            },
            "version": "0.0.0",
            "filename": "",
            "groupTitle": "Challenge"
        }, {
            "type": "get",
            "url": "regioes",
            "title": "Listar Regiões",
            "name": "retornar_regioes",
            "group": "Challenge",
            "success": {
                "examples": [
                    {
                        "title": "Success-Response:",
                        "content":"[\r\n  {\r\n    \"id\": 1,\r\n    \"regiao\": \"Norte\"\r\n  },\r\n  {\r\n    \"id\": 2,\r\n    \"regiao\": \"Nordeste\"\r\n  },\r\n  {\r\n    \"id\": 3,\r\n    \"regiao\": \"Sudeste\"\r\n  },\r\n  {\r\n    \"id\": 4,\r\n    \"regiao\": \"Sul\"\r\n  },\r\n  {\r\n    \"id\": 5,\r\n    \"regiao\": \"Centro-Oeste\"\r\n  }\r\n]",
                        "type": "json"
                    }
                ]
            },
            "error": {
                "examples": [
                    {
                        "title": "Error-Response:",
                        "content": "[]",
                        "type": "json"
                    }
                ]
            },
            "version": "0.0.0",
            "filename": "",
            "groupTitle": "Challenge"
        }, {
            "type": "get",
            "url": "municipios/{estado}",
            "title": "Listar Municipios por estado",
            "name": "retornar_municipios",
            "group": "Challenge", "parameter":  {
                "fields": {
                    "Parameter": [
                        {
                            "group": "Parameter",
                            "type": "string",
                            "optional": false,
                            "field": "estado",
                            "description": "<p>Estado dos municipios que serão listados</p>"
                        }
                    ]
                }
            },
            "success": {
                "examples": [
                    {
                        "title": "Success-Response:",
                        "content": "[\r\n  {\r\n    \"id\": 3098,\r\n    \"cod\": \"3200102\",\r\n    \"municipio\": \"Afonso Cl\u00E1udio\",\r\n    \"uf\": \"ES\"\r\n  },\r\n  {\r\n    \"id\": 3099,\r\n    \"cod\": \"3200136\",\r\n    \"municipio\": \"\u00C1guia Branca\",\r\n    \"uf\": \"ES\"\r\n  },\r\n  {\r\n    \"id\": 3100,\r\n    \"cod\": \"3200169\",\r\n    \"municipio\": \"\u00C1gua Doce do Norte\",\r\n    \"uf\": \"ES\"\r\n  },\r\n  {\r\n    \"id\": 3101,\r\n    \"cod\": \"3200201\",\r\n    \"municipio\": \"Alegre\",\r\n    \"uf\": \"ES\"\r\n  },\r\n  {\r\n    \"id\": 3102,\r\n    \"cod\": \"3200300\",\r\n    \"municipio\": \"Alfredo Chaves\",\r\n    \"uf\": \"ES\"\r\n  },\r\n  {\r\n    \"id\": 3103,\r\n    \"cod\": \"3200359\",\r\n    \"municipio\": \"Alto Rio Novo\",\r\n    \"uf\": \"ES\"\r\n  }]",
                        "type": "json"
                    }
                ]
            },
            "error": {
                "examples": [
                    {
                        "title": "Error-Response:",
                        "content": "[]",
                        "type": "json"
                    }
                ]
            },
            "version": "0.0.0",
            "filename": "",
            "groupTitle": "Challenge"
        }



] });