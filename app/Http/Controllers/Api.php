<?php

namespace App\Http\Controllers;

use App\Estado;
use App\Municipio;
use App\Regiao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Api extends Controller
{

    function sanitizeString($str) {
        $str = preg_replace('/[áàãâä]/ui', 'a', $str);
        $str = preg_replace('/[éèêë]/ui', 'e', $str);
        $str = preg_replace('/[íìîï]/ui', 'i', $str);
        $str = preg_replace('/[óòõôö]/ui', 'o', $str);
        $str = preg_replace('/[úùûü]/ui', 'u', $str);
        $str = preg_replace('/[ç]/ui', 'c', $str);
        // $str = preg_replace('/[,(),;:|!"#$%&/=?~^><ªº-]/', '_', $str);
        $str = preg_replace('/[^a-z0-9]/i', '_', $str);
        $str = preg_replace('/_+/', '_', $str);
        return $str;
    }

   public function findbyRegiao($regiao, $municipio){

       $municipios = Estado::query()->join('regiaos', 'regiaos.id', '=', 'estados.regiao')->join('municipios', 'municipios.uf', '=', 'estados.sigla')->where('regiaos.regiao', '=', $regiao)->get(['municipios.*']);
       $resultado = array();
       $resultCount = 0;
       for($i = 0; $i < sizeof($municipios); $i++ ){
           $pattern = explode('_',  strtolower(self::sanitizeString($municipio)));
           $find = explode('_',  strtolower(self::sanitizeString($municipios[$i]['municipio'])));
           $count_result = 0;
           for($j = 0; $j < sizeof($find); $j++){
               for($k = 0; $k < sizeof($pattern); $k++){
                   if($pattern[$k] == $find[$j]) {
                       $count_result++;
                   }
               }

           }

           if($count_result == sizeof($pattern))
           {

               $resultado[$resultCount] =  $municipios[$i];
               $resultCount++;
           }

           }

       return response()->json($resultado);
       }


       public function findbyEstado($estado, $municipio){
           $municipios = Estado::query()->join('municipios', 'municipios.uf', '=', 'estados.sigla')->where('estados.estado', '=', $estado)->get(['municipios.*']);
           $resultado = array();
           $resultCount = 0;

           for($i = 0; $i < sizeof($municipios); $i++ ){
               $pattern = explode('_', strtolower(self::sanitizeString($municipio)));
               $find = explode('_',  strtolower(self::sanitizeString($municipios[$i]['municipio'])));
               $count_result = 0;
                   for($j = 0; $j < sizeof($find); $j++){

                       for($k = 0; $k < sizeof($pattern); $k++){

                           if($pattern[$k] == $find[$j]){
                               $count_result++;
                           }
                       }
                   }
               if($count_result == sizeof($pattern))
               {
                   $resultado[$resultCount] =  $municipios[$i];
                   $resultCount++;
               }
           }
           return response()->json($resultado);
       }


       public function getEstados(){
        $estados = Estado::all();
        return $estados;

       }

       public function getRegioes(){

        $regioes = Regiao::all();
        return $regioes;

        }

        public function getMunicipio($estado){
            $municipios = Estado::query()->join('municipios', 'municipios.uf', '=', 'estados.sigla')->where('estados.sigla', '=', $estado)->get(['municipios.*']);

            return response()->json($municipios);
        }




}
