<?php

use Illuminate\Http\Request;



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

header("Access-Control-Allow-Origin: *");

Route::get('/regiao/{regiao}/{municipio}', 'Api@findbyRegiao');

Route::get('/estado/{estado}/{municipio}', 'Api@findbyEstado');

Route::get('/estados', 'Api@getEstados');

Route::get('/regioes', 'Api@getRegioes');

Route::get('/municipios/{estado}', 'Api@getMunicipio');




